from django.conf.urls import re_path
from .views import landingPage, loginPage, auth_login

urlpatterns = [
    re_path(r'^$', landingPage, name='index'),
    re_path(r'^login/', loginPage, name='login'),
    re_path(r'^auth-login/', auth_login, name='auth-login')
]