from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.contrib import messages
from django.db import connection
from simui_db.models import *

response = {
    'title': 'Sistem Informasi Mahasiswa UI (SIMUI)',
    'author': 'Basis Data Ganjil 2018/19',
}

# Create your views here.
def landingPage(request):
    if 'username' in request.session.keys():
        print("#Already logged in")
        return HttpResponseRedirect(reverse("main:index"))
    else:
        print("#Landing page is executed!")
        html = "landing_page.html"
        return render(request, html, response)

def loginPage(request):
    if 'username' in request.session.keys():
        print("#Already logged in")
        return HttpResponseRedirect(reverse("main:index"))
    else:
        print("#Login page is executed!")
        response['login_title'] = 'Login ke SIMUI'
        html = "login.html"
        return render(request, html, response)

def auth_login(request):
    print("#Auth Login")
    if request.method == "POST":
        user_login = request.POST["username"]
        user_password = request.POST["password"]

        if user_is_exist(user_login, user_password):
            print("#granted access")

            # userName = Pengguna.objects.raw("SELECT * FROM PENGGUNA WHERE username = %s AND password = %s", [user_login, user_password])
            # print(userName[0].nama)
            res = HttpResponseRedirect(reverse('main:index'))

            request.session['username'] = user_login
            request.session['password'] = user_password

            return res

        else:
            print("#not granted access")
            msg = "Username/Password Anda Salah atau Akun Tidak Ada"
            messages.error(request, msg)
            return HttpResponseRedirect(reverse('login:login'))

def user_is_exist(uname, in_pass):
    available_user = Pengguna.objects.raw("SELECT * FROM PENGGUNA WHERE username = %s AND password = %s", [uname, in_pass])
    return len(list(available_user)) != 0
