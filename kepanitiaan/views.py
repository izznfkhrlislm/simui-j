from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.contrib import messages
from django.db import connection
from simui_db.models import *
from .forms import TingkatanChoice, KategoriChoice, OrganisasiChoice

response = {
	'kepanitiaan_title': 'Organisasi - Sistem Informasi Mahasiswa UI (SIMUI)',
	'author': 'Basis Data Ganjil 2018/19',
	'tingkatan_dropdown' : TingkatanChoice(),
	'kategori_dropdown' : KategoriChoice(),
    'organisasi_dropdown': OrganisasiChoice(),
}

def list_of_kepanitiaan(request):
	list_kepanitiaan = list(PembuatEvent.objects.raw("SELECT * FROM pembuat_event pe, kepanitiaan k WHERE pe.id = k.id_kepanitiaan ORDER BY pe.id DESC"))

	response['list_kepanitiaan'] = list_kepanitiaan

	html = "list_kepanitiaan.html"
	return render(request, html, response)

def form_add_kepanitiaan(request):
	print("hehe")
	latest_id = list(PembuatEvent.objects.raw("SELECT * FROM pembuat_event ORDER BY id DESC"))[0].id
	id_id = int(latest_id[-2:]) + 1
	final_id = 'R00' + str(id_id)

	if request.method == 'POST':
		id = final_id
		id_organisasi = request.POST['id_organisasi']
		nama_kepanitiaan = request.POST['nama_kepanitiaan']
		tingkatan = request.POST['tingkatan']
		email = request.POST['email']
		website = request.POST['website']
		contact_person = request.POST['contact_person']
		kategori = request.POST['kategori']
		deskripsi = request.POST['deskripsi']

		with connection.cursor() as c:
			sql = "INSERT INTO pembuat_event (id, nama, email, alamat_website, tingkatan, kategori, deskripsi, contact_person, logo) VALUES ('%s','%s','%s','%s','%s','%s','%s','%s',NULL)" % (id, nama_kepanitiaan, email, website, tingkatan, kategori, deskripsi, contact_person)
			sql1 = "INSERT INTO kepanitiaan (id_kepanitiaan, id_organisasi) VALUES ('%s','%s')" % (id, id_organisasi)
			c.execute(sql)
			c.execute(sql1)

		return HttpResponseRedirect(reverse('kepanitiaan:index'))

	else:
		html = "add_kepanitiaan.html"
		return render(request, html, response)

def form_update_kepanitiaan(request, id_kepanitiaan):
	response['id_current'] = id_kepanitiaan
	if request.method == 'POST':
		id = id_kepanitiaan
		nama_kepanitiaan = request.POST['nama_kepanitiaan']
		tingkatan = request.POST['tingkatan']
		email = request.POST['email']
		website = request.POST['website']
		contact_person = request.POST['contact_person']
		kategori = request.POST['kategori']
		deskripsi = request.POST['deskripsi']

		with connection.cursor() as c:
			sql = "UPDATE pembuat_event SET nama = '%s', tingkatan = '%s', email = '%s', website = '%s', contact_person = '%s', kategori = '%s', deskripsi = '%s' WHERE id = '%s'" % (nama_kepanitiaan, email, website, tingkatan, kategori, deskripsi, contact_person, id)
			c.execute(sql)

		return HttpResponseRedirect(reverse('kepanitiaan:index'))
	else:
		html = "update_kepanitiaan.html"
		return render(request, html, response)

def delete_kepanitiaan(request, id_kepanitiaan):
	with connection.cursor() as c:
		c.execute("DELETE FROM kepanitiaan WHERE id_kepanitiaan= '%s'", [id_kepanitiaan])
		return HttpResponseRedirect(reverse('main:index'))