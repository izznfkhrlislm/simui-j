from django.urls import path
from .views import list_of_kepanitiaan, delete_kepanitiaan, form_add_kepanitiaan, form_update_kepanitiaan
urlpatterns = [
    path('', list_of_kepanitiaan, name='index'),
    path('add_kepanitiaan/', form_add_kepanitiaan, name='add_kepanitiaan'),
    path('update_kepanitiaan/<str:id_kepanitiaan>/', form_update_kepanitiaan, name='update_kepanitiaan'),
    path('delete_kepanitiaan/<str:id_kepanitiaan>/', delete_kepanitiaan, name='delete_kepanitiaan'),
]