from django import forms
from simui_db.models import *

TINGKATAN_CHOICES = []
KATEGORI_CHOICES = []
ORGANISASI_CHOICES = []

tingkatan_data = list(PembuatEvent.objects.raw("SELECT DISTINCT id, tingkatan FROM pembuat_event"))
kategori_data = list(PembuatEvent.objects.raw("SELECT DISTINCT id, kategori FROM pembuat_event"))
organisasi_data = list(PembuatEvent.objects.raw("SELECT DISTINCT id, nama FROM pembuat_event pe, organisasi o WHERE pe.id = o.id_organisasi"))

for i in range(len(tingkatan_data)):
    if (tingkatan_data[i].tingkatan, tingkatan_data[i].tingkatan) not in TINGKATAN_CHOICES:
        TINGKATAN_CHOICES.append((tingkatan_data[i].tingkatan, tingkatan_data[i].tingkatan))
for i in range(len(kategori_data)):
    if (kategori_data[i].kategori, kategori_data[i].kategori) not in KATEGORI_CHOICES:
        KATEGORI_CHOICES.append((kategori_data[i].kategori, kategori_data[i].kategori))

for i in range(len(organisasi_data)):
    if (organisasi_data[i].id, organisasi_data[i].nama) not in ORGANISASI_CHOICES:
        ORGANISASI_CHOICES.append((organisasi_data[i].id, organisasi_data[i].nama))

class OrganisasiChoice(forms.Form):
    organisasi = forms.ChoiceField(
        choices=ORGANISASI_CHOICES,
        required=True,
        widget=forms.Select(
            attrs={
                'class': 'btn btn-primary dropdown-toggle dropdown-toggle-split',
                'data-toggle': 'dropdown',
                'aria-expanded': 'false',
                'type': 'button',
            }
        )
    )

class TingkatanChoice(forms.Form):
    tingkatan = forms.ChoiceField(
        choices=TINGKATAN_CHOICES,
        required=True,
        widget=forms.Select(
            attrs={
                'class':'btn btn-primary dropdown-toggle dropdown-toggle-split',
                'data-toggle': 'dropdown',
                'aria-expanded': 'false',
                'type': 'button',
            }
        ),
    )

class KategoriChoice(forms.Form):
    kategori = forms.ChoiceField(
        choices=KATEGORI_CHOICES,
        required=True,
        widget=forms.Select(
            attrs={
                'class':'form-control',
            }
        ),
    )