from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.contrib import messages
from django.db import connection
from simui_db.models import *
from .forms import KategoriChoice, OrganisasiChoice, KepanitiaanChoice

response = {
	'event_title': 'Event - Sistem Informasi Mahasiswa UI (SIMUI)',
	'author': 'Basis Data Ganjil 2018/19',
	'kategori_dropdown' : KategoriChoice(),
    'organisasi_dropdown': OrganisasiChoice(),
	'kepanitiaan_dropdown': KepanitiaanChoice(),
}

def list_of_event(request):
	list_event = list(Event.objects.raw("SELECT * FROM event e, pembuat_event pe WHERE pe.id = e.id_pembuat_event ORDER BY e.id_event DESC"))

	response['list_event'] = list_event

	html = "list_event.html"
	return render(request, html, response)

def form_add_event(request):
	latest_id = list(Event.objects.raw("SELECT * FROM event ORDER BY id_event DESC"))[0].id_event
	id_id = latest_id+1
	final_id = id_id

	if request.method == 'POST':
		id_pembuat_event = ''
		id = final_id
		id_organisasi = request.POST['organisasi']
		id_kepanitiaan = request.POST['kepanitiaan']
		nama_event = request.POST['nama_event']
		tanggal = request.POST['tanggal']
		waktu = request.POST['waktu']
		nomor_kategori = request.POST['kategori']
		deskripsi = request.POST['deskripsi']
		kapasitas = request.POST['kapasitas']
		sifat_event = request.POST['sifat_event']
		harga_tiket = request.POST['harga_tiket']
		lokasi = request.POST['lokasi']

		if id_organisasi != '':
			id_pembuat_event = id_organisasi
		if id_kepanitiaan != '':
			id_pembuat_event = id_kepanitiaan

		with connection.cursor() as c:
			sql = "INSERT INTO event (id_pembuat_event, id_event, nama, tanggal, waktu, nomor_kategori, deskripsi_singkat, kapasitas, sifat_event, harga_tiket, lokasi) VALUES ('%s',%s,'%s','%s','%s','%s','%s',%s,'%s',%s,'%s')" % (id_pembuat_event, id, nama_event, tanggal, waktu, nomor_kategori, deskripsi, kapasitas, sifat_event, harga_tiket, lokasi)
			c.execute(sql)

		return HttpResponseRedirect(reverse('event:index'))

	else:
		html = "add_event.html"
		return render(request, html, response)

def form_update_event(request, id_event):
	response['id_current'] = id_event
	if request.method == 'POST':
		id_pembuat_event = ''
		id = id_event
		id_organisasi = request.POST['organisasi']
		id_kepanitiaan = request.POST['kepanitiaan']
		nama_event = request.POST['nama_event']
		tanggal = request.POST['tanggal']
		waktu = request.POST['waktu']
		nomor_kategori = request.POST['kategori']
		deskripsi = request.POST['deskripsi']
		kapasitas = request.POST['kapasitas']
		sifat_event = request.POST['sifat_event']
		harga_tiket = request.POST['harga_tiket']
		lokasi = request.POST['lokasi']

		if id_organisasi != '':
			id_pembuat_event = id_organisasi
		if id_kepanitiaan != '':
			id_pembuat_event = id_kepanitiaan

		with connection.cursor() as c:
			sql = "UPDATE event SET id_pembuat_event = '%s', nama = '%s', tanggal = '%s', waktu = '%s', nomor_kategori = '%s', deskripsi_singkat = '%s', kapasitas = %s, sifat_event = '%s', harga_tiket = %s, lokasi = '%s' WHERE id_event = %s" % (id_pembuat_event, nama_event, tanggal, waktu, nomor_kategori, deskripsi, kapasitas, sifat_event, harga_tiket, lokasi, id)
			c.execute(sql)

		return HttpResponseRedirect(reverse('event:index'))
	else:
		html = "update_event.html"
		return render(request, html, response)

def delete_event(request, id_event):
	with connection.cursor() as c:
		c.execute("DELETE FROM event WHERE id_event= %s", [id_event])
		return HttpResponseRedirect(reverse('event:index'))