from django import forms
from simui_db.models import *

KATEGORI_CHOICES = []
ORGANISASI_CHOICES = []
KEPANITIAAN_CHOICES = []

kategori_data = list(KategoriEvent.objects.raw("SELECT DISTINCT nomor, nama FROM kategori_event"))
organisasi_data = list(PembuatEvent.objects.raw("SELECT DISTINCT id, nama FROM pembuat_event pe, organisasi o WHERE pe.id = o.id_organisasi"))
kepanitiaan_data = list(PembuatEvent.objects.raw("SELECT DISTINCT id, nama FROM pembuat_event pe, kepanitiaan k WHERE pe.id = k.id_kepanitiaan"))


for i in range(len(kategori_data)):
	if (kategori_data[i].nomor, kategori_data[i].nama) not in KATEGORI_CHOICES:
		KATEGORI_CHOICES.append((kategori_data[i].nomor, kategori_data[i].nama))

for i in range(len(organisasi_data)):
	if (organisasi_data[i].id, organisasi_data[i].nama) not in ORGANISASI_CHOICES:
		ORGANISASI_CHOICES.append((organisasi_data[i].id, organisasi_data[i].nama))

for i in range(len(kepanitiaan_data)):
	if (kepanitiaan_data[i].id, kepanitiaan_data[i].nama) not in KEPANITIAAN_CHOICES:
		KEPANITIAAN_CHOICES.append((kepanitiaan_data[i].id, kepanitiaan_data[i].nama))

class KepanitiaanChoice(forms.Form):
	kepanitiaan = forms.ChoiceField(
		choices=KEPANITIAAN_CHOICES,
		required=False,
		widget=forms.Select(
			attrs={
				'class': 'btn btn-primary dropdown-toggle dropdown-toggle-split',
				'data-toggle': 'dropdown',
				'aria-expanded': 'false',
				'type': 'button',
			}
		)
	)

class OrganisasiChoice(forms.Form):
	organisasi = forms.ChoiceField(
		choices=ORGANISASI_CHOICES,
		required=False,
		widget=forms.Select(
			attrs={
				'class': 'btn btn-primary dropdown-toggle dropdown-toggle-split',
				'data-toggle': 'dropdown',
				'aria-expanded': 'false',
				'type': 'button',
			}
		)
	)

class KategoriChoice(forms.Form):
	kategori = forms.ChoiceField(
		choices=KATEGORI_CHOICES,
		required=True,
		widget=forms.Select(
			attrs={
				'class':'form-control',
			}
		),
	)