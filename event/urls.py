from django.urls import path
from .views import list_of_event, form_add_event, delete_event, form_update_event

urlpatterns = [
    path('', list_of_event, name='index'),
    path('add_event/', form_add_event, name='add_event'),
    path('update_event/<str:id_event>/', form_update_event, name='update_event'),
    path('delete_event/<str:id_event>/', delete_event, name='delete_event'),
]