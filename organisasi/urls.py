from django.urls import path
from .views import list_of_organisasi, delete_organisasi, form_add_organisasi, form_update_organisasi

urlpatterns = [
    path('', list_of_organisasi, name='index'),
    path('add_organisasi/', form_add_organisasi, name='add_organisasi'),
    path('update_organisasi/<str:id_organisasi>/', form_update_organisasi, name='update_organisasi'),
    path('delete_organisasi/<str:id_organisasi>/', delete_organisasi, name='delete_organisasi'),
]