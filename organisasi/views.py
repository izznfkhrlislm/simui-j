from django.shortcuts import render, get_object_or_404
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.contrib import messages
from django.db import connection
from simui_db.models import *
from .forms import TingkatanChoice, KategoriChoice

response = {
	'organisasi_title': 'Organisasi - Sistem Informasi Mahasiswa UI (SIMUI)',
	'author': 'Basis Data Ganjil 2018/19',
    'tingkatan_dropdown' : TingkatanChoice(),
    'kategori_dropdown' : KategoriChoice(),
}

def list_of_organisasi(request):
	list_organisasi = list(Organisasi.objects.raw("SELECT * FROM pembuat_event pe, organisasi o WHERE pe.id = o.id_organisasi ORDER BY pe.id DESC"))

	response['list_organisasi'] = list_organisasi

	html = "list_organisasi.html"
	return render(request, html, response)

def form_add_organisasi(request):
	print("hehe")
	latest_id = list(PembuatEvent.objects.raw("SELECT * FROM pembuat_event ORDER BY id DESC"))[0].id
	id_id = int(latest_id[-2:]) + 1
	final_id = 'R00' + str(id_id)

	if request.method == 'POST':
		id = final_id
		nama_organisasi = request.POST['nama_organisasi']
		tingkatan = request.POST['tingkatan']
		email = request.POST['email']
		website = request.POST['website']
		contact_person = request.POST['contact_person']
		kategori = request.POST['kategori']
		deskripsi = request.POST['deskripsi']

		with connection.cursor() as c:
			sql = "INSERT INTO pembuat_event (id, nama, email, alamat_website, tingkatan, kategori, deskripsi, contact_person, logo) VALUES ('%s','%s','%s','%s','%s','%s','%s','%s',NULL)" % (id, nama_organisasi, email, website, tingkatan, kategori, deskripsi, contact_person)
			sql1 = "INSERT INTO organisasi (id_organisasi) VALUES ('%s')" % (id)
			c.execute(sql)
			c.execute(sql1)

		return HttpResponseRedirect(reverse('organisasi:index'))

	else:
		html = "add_organisasi.html"
		return render(request, html, response)

def form_update_organisasi(request, id_organisasi):
	response['id_current'] = id_organisasi

	if request.method == 'POST':
		id = id_organisasi
		nama_organisasi = request.POST['nama_organisasi']
		tingkatan = request.POST['tingkatan']
		email = request.POST['email']
		website = request.POST['website']
		contact_person = request.POST['contact_person']
		kategori = request.POST['kategori']
		deskripsi = request.POST['deskripsi']

		with connection.cursor() as c:
			sql = "UPDATE pembuat_event SET nama = '%s', tingkatan = '%s', email = '%s', website = '%s', contact_person = '%s', kategori = '%s', deskripsi = '%s' WHERE id = '%s'" % (nama_organisasi, email, website, tingkatan, kategori, deskripsi, contact_person, id)
			c.execute(sql)

		return HttpResponseRedirect(reverse('organisasi:index'))
	else:
		html = "update_organisasi.html"
		return render(request, html, response)

def delete_organisasi(request, id_organisasi):
	with connection.cursor() as c:
		c.execute("DELETE FROM organisasi WHERE id_organisasi=%s", [id_organisasi])
		return HttpResponseRedirect(reverse('organisasi:index'))