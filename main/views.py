from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.urls import reverse

from simui_db.models import *

response = {
    'main_title': 'Sistem Informasi Mahasiswa UI (SIMUI)',
    'author': 'Basis Data Ganjil 2018/19',
}

def main_index(request):
    if 'username' in request.session.keys():
        print("main page is executed!")

        loggedInUserName = request.session['username']
        listOrganisasi = list(Organisasi.objects.raw("SELECT * FROM pembuat_event pe, organisasi o WHERE pe.id = o.id_organisasi ORDER BY pe.id DESC LIMIT 5"))
        listKepanitiaan = list(Kepanitiaan.objects.raw("SELECT * FROM pembuat_event pe, kepanitiaan k WHERE pe.id = k.id_kepanitiaan ORDER BY pe.id DESC LIMIT 5"))
        listEvent = list(Event.objects.raw("SELECT * FROM event e ORDER BY e.id_event DESC LIMIT 5"))

        isAdminList = list(Admin.objects.raw("SELECT * FROM admin WHERE username = %s", [loggedInUserName]))
        isNonAdminList = list(NonAdmin.objects.raw("SELECT * FROM non_admin WHERE username = %s", [loggedInUserName]))
        isMahasiswaList = list(Mahasiswa.objects.raw("SELECT * FROM mahasiswa WHERE username = %s", [loggedInUserName]))
        isDosenList = list(Dosen.objects.raw("SELECT * FROM dosen WHERE username = %s", [loggedInUserName]))
        isStaffList = list(Staff.objects.raw("SELECT * FROM staff WHERE username = %s", [loggedInUserName]))
        isGuestList = list(Guest.objects.raw("SELECT * FROM guest WHERE username = %s", [loggedInUserName]))

        isNonAdmin = False
        isAdmin = False
        isDosen = False
        isMahasiswa = False
        isStaff = False
        isGuest = False

        if len(isNonAdminList) != 0:
            isNonAdmin = True
        if len(isAdminList) != 0:
            isAdmin = True
        if len(isDosenList) != 0:
            isDosen = True
        if len(isMahasiswaList) != 0:
            isMahasiswa = True
        if len(isStaffList) != 0:
            isStaff = True
        if len(isGuestList) != 0:
            isGuest = True

        print(isAdmin)

        if isAdmin == True:
            response['is_admin'] = True
            response['role'] = 'Admin'
            response['full_name'] = 'Admin_'+loggedInUserName

        else:
            response['is_admin'] = False
            response['full_name'] = list(NonAdmin.objects.raw("SELECT * FROM non_admin WHERE username = %s", [loggedInUserName]))[0].nama
            response['email'] = list(NonAdmin.objects.raw("SELECT * FROM non_admin WHERE username = %s", [loggedInUserName]))[0].email
            response['no_telepon'] = list(NonAdmin.objects.raw("SELECT * FROM non_admin WHERE username = %s", [loggedInUserName]))[0].no_telepon
            response['role'] = 'Non Admin'

            if isMahasiswa == True:
                response['is_mahasiswa'] = True
                response['npm'] = list(Mahasiswa.objects.raw("SELECT * FROM mahasiswa WHERE username = %s", [loggedInUserName]))[0].npm
                response['role'] = 'Mahasiswa'

            if isDosen == True:
                response['is_dosen'] = True
                response['nidn'] = list(Dosen.objects.raw("SELECT * FROM dosen WHERE username = %s", [loggedInUserName]))[0].nidn
                print("hehehe")
                response['role'] = 'Dosen'

            if isStaff == True:
                response['is_staff'] = True
                response['nip'] = list(Staff.objects.raw("SELECT * FROM staff WHERE username = %s", [loggedInUserName]))[0].nip
                response['role'] = 'Staff'

            if isGuest == True:
                response['is_guest'] = True
                response['pekerjaan'] = list(Guest.objects.raw("SELECT * FROM guest WHERE username = %s", [loggedInUserName]))[0].pekerjaan
                response['alamat'] = list(Guest.objects.raw("SELECT * FROM guest WHERE username = %s", [loggedInUserName]))[0].alamat
                response['role'] = 'Guest'

        response['list_organisasi'] = listOrganisasi
        response['list_kepanitiaan'] = listKepanitiaan
        response['list_event'] = listEvent
        response['logged_in_user_name'] = loggedInUserName
        html = "main.html"

        return render(request, html, response)

    else:
        print("#Not logged in")
        return HttpResponseRedirect(reverse("login:index"))


def logout(request):
    request.session.flush()
    return HttpResponseRedirect(reverse('login:index'))