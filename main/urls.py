from django.urls import path
from .views import main_index, logout

urlpatterns = [
    path('', main_index, name='index'),
    path('logout/', logout, name='logout'),
]