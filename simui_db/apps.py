from django.apps import AppConfig


class SimuiDbConfig(AppConfig):
    name = 'simui_db'
