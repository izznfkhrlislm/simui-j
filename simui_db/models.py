# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey has `on_delete` set to the desired behavior.
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models


class Admin(models.Model):
    username = models.ForeignKey('Pengguna', models.DO_NOTHING, db_column='username', primary_key=True)

    class Meta:
        managed = False
        db_table = 'admin'


class AuthGroup(models.Model):
    name = models.CharField(unique=True, max_length=80)

    class Meta:
        managed = False
        db_table = 'auth_group'


class AuthGroupPermissions(models.Model):
    group = models.ForeignKey(AuthGroup, models.DO_NOTHING)
    permission = models.ForeignKey('AuthPermission', models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_group_permissions'
        unique_together = (('group', 'permission'),)


class AuthPermission(models.Model):
    name = models.CharField(max_length=255)
    content_type = models.ForeignKey('DjangoContentType', models.DO_NOTHING)
    codename = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'auth_permission'
        unique_together = (('content_type', 'codename'),)


class AuthUser(models.Model):
    password = models.CharField(max_length=128)
    last_login = models.DateTimeField(blank=True, null=True)
    is_superuser = models.BooleanField()
    username = models.CharField(unique=True, max_length=150)
    first_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=150)
    email = models.CharField(max_length=254)
    is_staff = models.BooleanField()
    is_active = models.BooleanField()
    date_joined = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'auth_user'


class AuthUserGroups(models.Model):
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)
    group = models.ForeignKey(AuthGroup, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_user_groups'
        unique_together = (('user', 'group'),)


class AuthUserUserPermissions(models.Model):
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)
    permission = models.ForeignKey(AuthPermission, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_user_user_permissions'
        unique_together = (('user', 'permission'),)


class DjangoAdminLog(models.Model):
    action_time = models.DateTimeField()
    object_id = models.TextField(blank=True, null=True)
    object_repr = models.CharField(max_length=200)
    action_flag = models.SmallIntegerField()
    change_message = models.TextField()
    content_type = models.ForeignKey('DjangoContentType', models.DO_NOTHING, blank=True, null=True)
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'django_admin_log'


class DjangoContentType(models.Model):
    app_label = models.CharField(max_length=100)
    model = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'django_content_type'
        unique_together = (('app_label', 'model'),)


class DjangoMigrations(models.Model):
    app = models.CharField(max_length=255)
    name = models.CharField(max_length=255)
    applied = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'django_migrations'


class DjangoSession(models.Model):
    session_key = models.CharField(primary_key=True, max_length=40)
    session_data = models.TextField()
    expire_date = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'django_session'


class Dosen(models.Model):
    username = models.ForeignKey('NonAdmin', models.DO_NOTHING, db_column='username', primary_key=True)
    nidn = models.CharField(max_length=20)

    class Meta:
        managed = False
        db_table = 'dosen'


class Event(models.Model):
    id_event = models.IntegerField(primary_key=True)
    id_pembuat_event = models.ForeignKey('PembuatEvent', models.DO_NOTHING, db_column='id_pembuat_event')
    nama = models.CharField(max_length=50)
    tanggal = models.DateField()
    waktu = models.TimeField()
    kapasitas = models.IntegerField()
    harga_tiket = models.IntegerField()
    lokasi = models.TextField()
    sifat_event = models.CharField(max_length=10)
    nomor_kategori = models.ForeignKey('KategoriEvent', models.DO_NOTHING, db_column='nomor_kategori', blank=True, null=True)
    deskripsi_singkat = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'event'
        unique_together = (('id_event', 'id_pembuat_event'),)


class Guest(models.Model):
    username = models.ForeignKey('NonAdmin', models.DO_NOTHING, db_column='username', primary_key=True)
    pekerjaan = models.CharField(max_length=50)
    alamat = models.TextField()

    class Meta:
        managed = False
        db_table = 'guest'


class KategoriEvent(models.Model):
    nomor = models.CharField(primary_key=True, max_length=5)
    nama = models.CharField(max_length=50)

    class Meta:
        managed = False
        db_table = 'kategori_event'


class Kepanitiaan(models.Model):
    id_kepanitiaan = models.ForeignKey('PembuatEvent', models.DO_NOTHING, db_column='id_kepanitiaan', primary_key=True)
    id_organisasi = models.ForeignKey('Organisasi', models.DO_NOTHING, db_column='id_organisasi')

    class Meta:
        managed = False
        db_table = 'kepanitiaan'
        unique_together = (('id_kepanitiaan', 'id_organisasi'),)


class Mahasiswa(models.Model):
    username = models.ForeignKey('NonAdmin', models.DO_NOTHING, db_column='username', primary_key=True)
    npm = models.CharField(max_length=15)

    class Meta:
        managed = False
        db_table = 'mahasiswa'


class NonAdmin(models.Model):
    username = models.ForeignKey('Pengguna', models.DO_NOTHING, db_column='username', primary_key=True)
    email = models.CharField(max_length=50)
    nama = models.CharField(max_length=50)
    no_telepon = models.CharField(max_length=20)

    class Meta:
        managed = False
        db_table = 'non_admin'


class OpenRecruitment(models.Model):
    id_oprec = models.IntegerField(primary_key=True)
    id_kepanitiaan = models.ForeignKey(Kepanitiaan, models.DO_NOTHING, db_column='id_kepanitiaan')
    id_organisasi = models.CharField(max_length=5)
    nama = models.CharField(max_length=50)
    tanggal_dibuka = models.DateField()
    tanggal_ditutup = models.DateField()
    berkas_dibutuhkan = models.TextField(blank=True, null=True)
    persyaratan = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'open_recruitment'
        unique_together = (('id_oprec', 'id_kepanitiaan', 'id_organisasi'),)


class Organisasi(models.Model):
    id_organisasi = models.ForeignKey('PembuatEvent', models.DO_NOTHING, db_column='id_organisasi', primary_key=True)

    class Meta:
        managed = False
        db_table = 'organisasi'


class PembuatEvent(models.Model):
    id = models.CharField(primary_key=True, max_length=5)
    nama = models.CharField(max_length=50)
    email = models.CharField(max_length=50)
    alamat_website = models.CharField(max_length=50, blank=True, null=True)
    tingkatan = models.CharField(max_length=50)
    kategori = models.CharField(max_length=50)
    logo = models.TextField(blank=True, null=True)
    deskripsi = models.TextField(blank=True, null=True)
    contact_person = models.CharField(max_length=20)

    class Meta:
        managed = False
        db_table = 'pembuat_event'


class PendaftaranEvent(models.Model):
    id_event = models.ForeignKey(Event, models.DO_NOTHING, db_column='id_event', primary_key=True)
    id_pembuat_event = models.CharField(max_length=5)
    username = models.ForeignKey(NonAdmin, models.DO_NOTHING, db_column='username')

    class Meta:
        managed = False
        db_table = 'pendaftaran_event'
        unique_together = (('id_event', 'id_pembuat_event', 'username'),)


class PendaftaranOprec(models.Model):
    id_oprec = models.ForeignKey(OpenRecruitment, models.DO_NOTHING, db_column='id_oprec', primary_key=True)
    id_organisasi = models.CharField(max_length=5)
    id_kepanitiaan = models.CharField(max_length=5)
    id_mhs = models.ForeignKey(Mahasiswa, models.DO_NOTHING, db_column='id_mhs')
    divisi = models.CharField(max_length=100)
    motivasi = models.TextField()
    tugas = models.TextField(blank=True, null=True)
    swot = models.TextField()
    berkas = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pendaftaran_oprec'
        unique_together = (('id_oprec', 'id_kepanitiaan', 'id_organisasi', 'id_mhs'),)


class Pengguna(models.Model):
    username = models.CharField(primary_key=True, max_length=15)
    password = models.CharField(max_length=50)

    class Meta:
        managed = False
        db_table = 'pengguna'


class PengisiAcara(models.Model):
    id_event = models.ForeignKey(Event, models.DO_NOTHING, db_column='id_event', primary_key=True)
    id_pembuat_event = models.CharField(max_length=5)
    nama_pengisi_acara = models.CharField(max_length=20)

    class Meta:
        managed = False
        db_table = 'pengisi_acara'
        unique_together = (('id_event', 'id_pembuat_event', 'nama_pengisi_acara'),)


class Staff(models.Model):
    username = models.ForeignKey(NonAdmin, models.DO_NOTHING, db_column='username', primary_key=True)
    nip = models.CharField(max_length=20)

    class Meta:
        managed = False
        db_table = 'staff'
